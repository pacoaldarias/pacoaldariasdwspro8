<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Asignatura {

//Propiedades
    private $id;
    private $nombre;
    private $horas;
    private $profesor; // Id Profesor

//Constructor

    public function __construct($id, $nombre, $horas, $profesor) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->horas = $horas;
        $this->profesor = $profesor;
    }

//Metodos
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getHoras() {
        return $this->horas;
    }

    public function setHoras($horas) {
        $this->horas = $horas;
    }

    public function getProfesor() {
        return $this->profesor;
    }

    public function setProfesor($profesor) {
        $this->profesor = $profesor;
    }

}

?>
