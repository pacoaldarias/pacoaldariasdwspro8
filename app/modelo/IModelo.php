<?php

interface IModelo {

    public function instalar();

    public function calculaidmaxprofesor();

    public function calculaidmaxasignatura();

    public function getProfesores();

    public function getProfesor($profesor_);

    public function grabarProfesor($profesor);

    public function grabarAsignatura($asignatura);

    public function getAsignaturas();
}
