<?php

// app/Config.php

class Config {

    static public $titulo = "Gestión de Asignaturas";
    static public $autor = "Paco Aldarias";
    static public $fecha = "09/01/2017";
    static public $empresa = "CEEDCV";
    static public $curso = "2016-17";
    static public $tema = "Tema 7. SESIONES";
    static public $modelo = "mysql";
    static public $bdhostname = "localhost";
    static public $bdnombre = "ceedcv";
    static public $bdusuario = "alumno";
    static public $bdclave = "alumno";
    static public $mvc_vis_css = "estilo.css";

}
?>

