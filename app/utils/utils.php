<?php 

function recoge($name) { 
	return strip_tags($_REQUEST[$name]); 
}

function sinTildes($sCadena) {
	$aVocalesSin = array("a","e","i","o","u","A","E","I","O","U");
	$aVocalesCon = array("á","é","í","ó","ú","Á","É","Í","Ó","Ú");
	return str_ireplace($aVocalesCon,$aVocalesSin,$sCadena);
}

function validarUsuario($cadena) {
	$sExpresion = "/^[A-Za-zñÑ0-9\-\_]{1,20}$/";
	if (preg_match($sExpresion,$cadena)) {
		return true;
	} else {
		return false;
	}
}

function validarClave($cadena) {
	$sExpresion = "/^[A-Za-zñÑ0-9\-\_\!\#]{1,32}$/";
	if (preg_match($sExpresion,$cadena)) {
		return true;
	} else {
		return false;
	}
}

function validarNombre($cadena) {
	$aVocalesSin = array("a","e","i","o","u","A","E","I","O","U");
	$aVocalesCon = array("á","é","í","ó","ú","Á","É","Í","Ó","Ú");
	$cadena = str_ireplace($aVocalesCon,$aVocalesSin,$cadena);
	$sExpresion = "/^[A-Za-zñÑ\s]{1,40}$/";
	if (preg_match($sExpresion,$cadena)) {
		return true;
	} else {
		return false;
	}
}

function validarEmail($cadena) {
	$sExpresion = "/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/";
	if (preg_match($sExpresion,$cadena)) {
		return true;
	} else {
		return false;
	}
}

function mensaje($tipo,$url,$msj) {
	$tipo = strtolower($tipo);
	switch ($tipo) {
		case "error":
			echo "<a href='$url'><i class='mdi mdi-close cerrarBusq error'></i></a><div class='msj error'>$msj</div>";
		break;
		case "correcto":
			echo "<a href='$url'><i class='mdi mdi-close cerrarBusq correcto'></i></a><div class='msj correcto'>$msj</div>";
		break;
	}
}

function transcurido($time) {
	$transcurido = time()-$time;
	$tc['minutos'] = $transcurido/60;
	$tc['horas'] = $transcurido/3600;
	$tc['dias'] = $transcurido/86400;
	$tc['meses'] = $transcurido/'2629743,83';
	$tc['años'] = $transcurido/31556926;
	$plu['minutos'] = (intval($tc['minutos'])==1) ? NULL : 's';
	$plu['horas'] = (intval($tc['horas'])==1) ? NULL : 's';
	$plu['dias'] = (intval($tc['dias'])==1) ? NULL : 's';
	$plu['meses'] = (intval($tc['meses'])==1) ? NULL : 's';
	$plu['años'] = (intval($tc['años'])==1) ? NULL : 's';
	$frase = "";
	$frase = ($transcurido<60 AND $transcurido>0) ? 'menos de un minuto' : $frase;
	$frase = ($transcurido>60 AND $transcurido<3600) ? intval($tc['minutos']).' minuto'.$plu['minutos'] : $frase;
	$frase = ($transcurido>3600 AND $transcurido<86400) ? intval($tc['horas']).' hora'.$plu['horas'] : $frase;
	$frase = ($transcurido>86000 AND $transcurido<'2629743,83') ? intval($tc['dias']).' dia'.$plu['dias'] : $frase;
	$frase = ($transcurido>'2629743,83' AND $transcurido<31556926) ? intval($tc['meses']).' mese'.$plu['meses'] : $frase;
	$frase = ($transcurido>31556926 AND $transcurido<315569260) ? intval($tc['años']).' año'.$plu['años'] : $frase;
	$frase = ($transcurido>3155692600) ? 'mas de 10 años' : $frase;
	return $frase;
}

function minutosAHoras($minutos) {
	$horas = floor($minutos / 60);
	$minutos = $minutos%60;
	if ($minutos == 0) {
		return $horas."h";
	} else {
		return $horas."h ".$minutos."m";
	}
}

function crypt_blowfish($password, $digito=17) {
	$set_salt = './1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	$salt = sprintf('$2a$%02d$', $digito);
	for($i = 0; $i < 22; $i++) {
		$salt .= $set_salt[mt_rand(0, 63)];
	}
	$pass = crypt($password, $salt);
	return $pass;
}
?>