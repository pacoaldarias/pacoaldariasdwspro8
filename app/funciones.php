<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once __DIR__ . '/Config.php';
require_once __DIR__ . '/modelo/ModeloFichero.php';
require_once __DIR__ . '/modelo/ModeloMysql.php';

function cabecera() {
    echo "<h1>" . Config::$titulo . "</h1><hr/>\n";
}

function pie() {
    echo "<hr/><pre>" . Config::$empresa . " " . Config::$autor . " ";
    echo Config::$curso . " " . Config::$fecha . "</pre>\n";
}

function inicio() {
    echo "<align='right'><a href = 'inicio.php'>Inicio</a> </align>\n";
}

function recoge($campo) {
    if (isset($_REQUEST[$campo])) {
        $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
    } else {
        $valor = "";
    };
    return $valor;
}

function obtenerModelo() {

    $tipo = Config::$modelo;
    switch ($tipo) {
        case 'fichero':
            $modelo = new ModeloFichero();
            break;
        case 'mysql':
            $modelo = new ModeloMysql(Config::$bdnombre, Config::$bdusuario, Config::$bdclave, Config::$bdhostname);
            break;
    }
    return $modelo;
}
?>

