<?php ob_start() ?>

<?php if (isset($params['mensaje'])) : ?>
    <b><span style="color: red;"><?php echo $params['mensaje'] ?></span></b>
    <?php endif; ?>

<br/>

<p>Documentación por tema:</p>

<ul>
    <li><a href="https://drive.google.com/file/d/0B3UbpRtIyr6qRnRyemlmMERoZFk/view?usp=sharing.pdf">Tema5. POO. Enunciado </a> </li>
    <li><a href="https://drive.google.com/file/d/0B3UbpRtIyr6qelZZTEYwSlFEZ1E/view?usp=sharing">Tema6. MYSQL. Enunciado </a> </li>
    <li><a href="https://drive.google.com/file/d/0B3UbpRtIyr6qZVd1MDhQMHpPV0E/view?usp=sharing">Tema7. Sesiones. Enunciado </a> </li>
    <li><a href="https://drive.google.com/file/d/0B3UbpRtIyr6qVUJBN1FpMEpRS0U/view?usp=sharing">Tema8. MVC. Enunciado </a> </li>
</ul>

<p>Documentación de este proyecto:</p>
<ul>
    <li><a href="../docs/DWSProyecto1Documentacion.pdf">Documentación </a> </li>
</ul>


<?php $contenido = ob_get_clean() ?>

<?php include 'layout.php' ?>