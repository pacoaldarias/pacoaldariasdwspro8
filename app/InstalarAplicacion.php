<?php
session_start();
if (!$_SESSION['SesionValida']) {
    header("Location: index.php");
}
include_once("funciones.php");
include_once("Modelo.php");
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        cabecera();

        $modelo = obtenerModelo();
        $modelo->instalar();

        pie();
        inicio();
        ?>
    </body>
</html>
