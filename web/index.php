<?php

// web/index.php

error_reporting(E_ALL);
ini_set('display_errors', '1');

// carga del modelo y los controladores
require_once __DIR__ . '/../app/Config.php';
require_once __DIR__ . '/../app/Controller.php';
require_once __DIR__ . '/../app/modelo/IModelo.php';


// enrutamiento
$map = array(
  'login' => array('controller' => 'Controller', 'action' => 'login'),
  'inicio' => array('controller' => 'Controller', 'action' => 'inicio'),
  'profesor' => array('controller' => 'Controller', 'action' => 'profesor'),
  'asignatura' => array('controller' => 'Controller', 'action' => 'asignatura'),
  'salir' => array('controller' => 'Controller', 'action' => 'salir'),
);

// Parseo de la ruta
if (isset($_GET['ctl'])) {
    if (isset($map[$_GET['ctl']])) {
        $ruta = $_GET['ctl'];
    } else {
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>' .
        $_GET['ctl'] .
        '</p></body></html>';
        exit;
    }
} else {
    $ruta = 'login';
}

$controlador = $map[$ruta];
// Ejecución del controlador asociado a la ruta

if (method_exists($controlador['controller'], $controlador['action'])) {
    call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {

    header('Status: 404 Not Found');
    echo '<html><body><h1>Error 404: El controlador <i>' .
    $controlador['controller'] .
    '->' .
    $controlador['action'] .
    '</i> no existe</h1></body></html>';
}